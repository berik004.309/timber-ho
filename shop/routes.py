import os
from flask import render_template, url_for, request, redirect, flash, session
from shop import app, db
from shop.models import Customer, Basket, Item, Review
from shop.forms import RegistrationForm, LoginForm, ReviewForm, PaymentForm as CheckoutForm
from flask_login import login_user, current_user, logout_user, login_required

##def sort_from_table(items):
##    
##    for i in range(len(items)):
##        print(items[i])
##        temp = items[i].price
##        for n in range(i-1,len(items)):
##            if items[i].price > items[n].price:
##                items[n] = items[i]
##                items[i] = temp
##    return items
##                

@app.route("/")
@app.route("/home")
def home():
    items = Item.query.all()
    return render_template('home.html', items=items, title='A large amount of wood')


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        if len(form.password.data) < 1:
            user = Customer(name=form.name.data, email=form.email.data, password=form.password.data)
        else:
            user = Customer(name=form.name.data, email=form.email.data, password="password")
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created.  You can now log in.')
        return redirect(url_for('home'))

    return render_template('register.html', title='Register', form=form)

@app.route("/item/<int:item_id>", methods=['GET', 'POST'])
def item(item_id):
    print(item_id)
    items = Item.query.get_or_404(item_id)
    review = Review.query.all()
    form = ReviewForm()
    if not current_user.is_authenticated:
        flash("Please register to buy items")
        return redirect(url_for('item/'+str(item_id)))
    else:
        user_n = current_user.name
        user_id = Customer.query.filter_by(id=user_n)
        if request.method == 'POST':
            UsrRev = Review(itemid=item_id,rev_text=form.rev_text.data)
            db.session.add(UsrRev)
            db.session.commit()
            flash('Your review has been submitted.')
            return redirect(url_for('item/'+str(item_id)))
    
    return render_template('item.html', item=items,rev=review,form=form)


@app.route("/login", methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = Customer.query.filter_by(email=form.email.data).first()

        if user.verify_password(form.password.data):
            login_user(user)
            flash('You are now logged in.')
            return redirect(url_for('home'))
        else:
            flash('Invalid username or password.')

        return render_template('login.html', form=form)

    return render_template('login.html', title='Login', form=form)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/add_to_cart/<int:item_id>")
def add_to_basket(item_id):
    if "cart" not in session:
        session["cart"] = []

    
    session["cart"].append(item_id)
    bask = Basket(custid=current_user.id,itemid=item_id,volume=1)
    db.session.add(bask)
    db.session.commit()

    flash("The TIMBER is added to your shopping cart!")
    return redirect("/cart")



@app.route("/cart", methods=['GET', 'POST'])
def cart_display():
    if "cart" not in session:
        flash('There is nothing in your cart.')
        return render_template("cart.html", display_cart = {}, total = 0)
    else:
        woods = session["cart"]
        cart = {}

        total_price = 0
        total_quantity = 0
        for wood in woods:
            item = Item.query.get_or_404(wood)

            total_price += item.price
            if item.id in cart:
                cart[item.id]["quantity"] += 1
            else:
                cart[item.id] = {"quantity":1, "title": item.name, "price":item.price}
            total_quantity = sum(item['quantity'] for item in cart.values())


        return render_template("cart.html", title='Your Shopping Cart', display_cart = cart, total = total_price, total_quantity = total_quantity)

    return render_template('cart.html')


@app.route("/checkout", methods=['GET', 'POST'])
def checkout_display():
    form = CheckoutForm()
    return render_template('checkout.html', form=form)


@app.route("/delete_book/<int:item_id>", methods=['GET', 'POST'])
def delete_item(item_id):
    if "cart" not in session:
        session["cart"] = []

    session["cart"].remove(item_id)
    Basket.query.filter_by(itemid=item_id).delete()
    db.session.commit()

    flash("The TIMBER has been removed from your shopping cart!")

    session.modified = True

    return redirect("/cart")
